# Elasticsearch Lab

Elasticsearch นั้นเป็นได้ทั้ง Database และ Search engine ในเวลาเดียวกัน เราสามารถใช้มันเก็บข้อมูลโดยกระจายข้อมูลให้อยู่หลายๆ เครื่องหรือที่เรียกว่า Node ใน Cluster ของเราได้ โดยแต่ละ Node ของ Elasticsearch ที่แบ่งกันถือข้อมูลอยู่นั้น จะมีความสามารถในการ Search ไม่ว่าจะเป็นการ query ง่ายๆ ไปจนถึง Full text search แบบซับซ้อน โดยใช้ resource ในเครื่องของตนเพื่อช่วยกันค้นหาข้อมูล ทำให้การ search นั้นเสร็จได้ในเวลาอันรวดเร็ว และเมื่อไหร่ที่รู้สึกว่าการ search เริ่มใช้เวลานานเพราะข้อมูลมีจำนวนมากขึ้น เราก็สามารถเพิ่ม Node ใหม่เข้าไปใน Cluster ได้ โดย Elasticsearch จะกระจายข้อมูลไปยังเครื่องใหม่ให้อัตโนมัติ ทำให้เราสามารถ Scale up ตามปริมาณการใช้งานที่จะเกิดขึ้นในอนาคตได้

ที่มา : [Elasticsearch ตั้งแต่เริ่มต้นยันใช้งานได้จริง](https://medium.com/insightera/%E0%B9%80%E0%B8%A3%E0%B8%B4%E0%B9%88%E0%B8%A1%E0%B8%95%E0%B9%89%E0%B8%99%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%88%E0%B8%B1%E0%B8%94%E0%B9%80%E0%B8%81%E0%B9%87%E0%B8%9A%E0%B8%82%E0%B9%89%E0%B8%AD%E0%B8%A1%E0%B8%B9%E0%B8%A5%E0%B8%82%E0%B8%99%E0%B8%B2%E0%B8%94%E0%B9%83%E0%B8%AB%E0%B8%8D%E0%B9%88%E0%B8%94%E0%B9%89%E0%B8%A7%E0%B8%A2-elasticsearch-4d70dbe6a79d)

**ความรู้เบื้องต้น**
- [Git](https://git-scm.com/docs)
- [Docker](https://docs.docker.com/get-started/)

**ซอฟต์แวร์ที่ใช้**
- [Git](https://github.com/git-guides/install-git)
- [Docker](https://docs.docker.com/engine/install/debian/)
- [Docker Compose](https://docs.docker.com/compose/install/compose-plugin/#install-using-the-repository)
- [Elasticsearch](https://www.docker.elastic.co/r/elasticsearch)
- [Kibana](https://www.docker.elastic.co/r/kibana)
- [Filebeat](https://www.docker.elastic.co/r/beats/filebeat) (option)

![dashboard](/screenshot/dashboard-01.jpg)

**กำหนดการ**
- 09:00 - 10:00 [การติดตั้ง](https://gitlab.com/samakida/elk8-lab#การติดตั้ง) และ [ติดตามความเคลื่อนไหว](https://gitlab.com/samakida/elk8-lab#ติดตามความเคลื่อนไหว-option)
- 10:00 - 10:15 พักรับประทานอาหารว่าง
- 10:15 - 12:00 การตั้งค่า [Elasticsearch](https://gitlab.com/samakida/elk8-lab#elasticsearch), [Kibana](https://gitlab.com/samakida/elk8-lab#kibana), [Filebeat](https://gitlab.com/samakida/elk8-lab#filebeat), [Index pettern](https://gitlab.com/samakida/elk8-lab#index-pettern), [Data pipeline](https://gitlab.com/samakida/elk8-lab#data-pipeline), [Index template](https://gitlab.com/samakida/elk8-lab#index-template)
- 12:00 - 13:00 พักรับประทานอาหารกลางวัน
- 13:00 - 14:00 [การค้นหาและกรองข้อมูล](https://gitlab.com/samakida/elk8-lab#การค้นหาและกรองข้อมูล)
- 14:00 - 14:15 พักรับประทานอาหารว่าง
- 14:15 - 16:00 [การแสดงผล](https://gitlab.com/samakida/elk8-lab#การแสดงผล)

## การติดตั้ง
```
git clone https://gitlab.com/samakida/elk8-lab.git
cd elk8-lab
cp .env.example .env
cp filebeat.yml.example filebeat.yml
docker-compose up -d
```

## ติดตามความเคลื่อนไหว (option)
```
docker-compose logs -f
```

## การตั้งค่า

### Elasticsearch

- [ ] elasticsearch-reset-password
```
docker exec -it elk8-lab-elasticsearch-1 /usr/share/elasticsearch/bin/elasticsearch-reset-password -u elastic
```

- [ ] elasticsearch-create-enrollment-token
```
docker exec -it elk8-lab-elasticsearch-1 /usr/share/elasticsearch/bin/elasticsearch-create-enrollment-token -s kibana
```

### Kibana

![Kibana](/screenshot/kibana-01.jpg)

- [ ] Enrollment
```
docker exec -it elk8-lab-kibana-1 /usr/share/kibana/bin/kibana-verification-code
```

### Filebeat
- [ ] Input TCP/UDP
- [ ] Output elasticsearch

## Index pettern

![Index pettern](/screenshot/index-pettern-01.jpg)

- [ ] Create
- [ ] View

## Data pipeline

![Data pipeline](/screenshot/data-pipeline-01.jpg)

- [ ] Grok Debugger
- [ ] Processor
>>>
- [Grok](https://www.alibabacloud.com/help/en/log-service/latest/grok-patterns#section-mt7-eyb-aet) ([Regular expression](https://regex101.com/))
- [GeoIP](https://www.elastic.co/guide/en/elasticsearch/reference/current/geoip-processor.html) (option)
- [Append](https://www.elastic.co/guide/en/elasticsearch/reference/8.2/grok-processor.html) (option)
>>>
- [ ] Failure processor (option)
>>>
- [Append](https://www.elastic.co/guide/en/elasticsearch/reference/8.2/ingest.html#handling-pipeline-failures) (option)
>>>

## Index template

![Index template](/screenshot/index-template-01.jpg)

- [ ] [Index settings](https://www.elastic.co/guide/en/elasticsearch/reference/current/ingest.html#set-default-pipeline)
- [ ] Mapping

## การค้นหาและกรองข้อมูล

![search](/screenshot/search-01.jpg)

- [ ] Search
- [ ] Fillter

## การแสดงผล

![dashboard](/screenshot/dashboard-01.jpg)

- [ ] Time series
- [ ] Metric
- [ ] Top N
- [ ] Gauge
- [ ] Pie
- [ ] Map (option)

# Option
- [Virtual memory](https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html)
```
vm.max_map_count=262144
```

## ผู้จัดทำ
[Somkid Charoenchai](https://gitlab.com/samakida) (s.charoenchai@gmail.com) <br>
ทีมพัฒนา SRAN ([บ.โกลบอลเทคโนโลยี อินทิเกรเทด จำกัด](https://www.gbtech.co.th/))
